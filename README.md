# manjaro-i3 config

place to keep system configuration for T490

[iso link](https://manjaro.org/downloads/community/i3/)

## Things done manually

* sudo pacman -S autorandr
* `xrandr --auto --output DP2-3 --above eDP1` place the docked monitor above the first
* `autorandr --save mobile` not sure how to do this automatically
* `autorandr --save docked` not sure how to do this automatically
* set dpi to 144 in .XResources
* configured firefox to replace palemoon in .i3/config
* removed palemoon
* add pcspkr to /etc/modprobe.d/blacklist
